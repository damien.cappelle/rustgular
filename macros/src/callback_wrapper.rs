
use proc_macro2::TokenStream;
use quote::{quote, ToTokens};
use syn::Expr;
pub struct CallbackWrapper<'a, U: ToTokens> {
    pub content: &'a Expr,
    pub argument_type: &'a U
}

impl<'a, U: ToTokens> ToTokens for CallbackWrapper<'a, U> {
    fn to_tokens(&self, tokens: &mut TokenStream) {
        let content = self.content;
        let argument_type = self.argument_type;

        let block = crate::block_executor::BlockExecutor{
            content: content,
            arc_name: &Expr::Verbatim(quote!{t})
        };

        tokens.extend(quote!{
            rustgular::Rc::from(move |
                t: rustgular::Arc<rustgular::Mutex<dyn rustgular::Callbackable>>, 
                event: #argument_type 
            | {
                #block
            })
        });
    }
}
