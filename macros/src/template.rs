
use std::collections::HashMap;

use quote::{ToTokens, quote};
use proc_macro2::TokenStream;
use syn::{Expr, Ident, parse_str, Type};
use regex::Regex;

use crate::{view_child::ViewChild, html_text::HtmlText};
use crate::{dom_analysis, callback_wrapper};
// use crate::macros::generate_ca;
/// Type of template
enum TemplateType {
    /// root template to be rendered
    Root,
    /// Tag content to be rendered:
    /// * name: tag name
    /// * options: string containing all other stuff 
    Tag{name: String, options: HashMap<String, String>, viewchild_tag: Option<String>}
}

/// Represent a template (or piece of template)
pub struct Template {
    template_type: TemplateType,
    content: Vec<dom_analysis::Node>,
    component_name: Ident,
    pub looked_for_view_childs: Vec<ViewChild>
}


impl ToTokens for Template {
    fn to_tokens(&self, tokens: &mut proc_macro2::TokenStream) {

        let render_content = self.render_inner_html();

        // If we are a tag, handle what should be handled
        if let TemplateType::Tag{name, options, viewchild_tag} = &self.template_type {

            // Dirty but `to_tokens` imposes `&self` (immutable)
            let mut options = options.clone();

            let is_component = name.len() >= 4 && &name[0..4] == "app-";

            let mut function_content = quote!{};

            let if_pattern = self.render_if_pattern(&mut options);

            function_content.extend(self.render_if_condition(&mut options));


            let xml_namespace = match self.get_namespace(&mut options) {
                Some(op) => quote!(Some(#op)),
                None => quote!(None)
            };

            if is_component {
                // `render_callbacks` and `render_binded_options` are called inside `render_child_component` for components
                // Nice refactoring: create equivalent function `render_html_component`
                function_content.extend(self.render_child_component(name.to_string(), &mut options));

            } else {
                function_content.extend(self.render_callbacks(&mut options, is_component));
                function_content.extend(self.render_binded_options(&mut options, false));
            }

            // Childview callbac
            let viewchild_callback = if let Some(viewchild_tag) = viewchild_tag {
                if let Some(viewchild) = self.looked_for_view_childs.iter().find(|vc| &vc.name == viewchild_tag) {
                    quote!{#viewchild}
                } else {quote!{}}
            } else {quote!{}};

            let mut internal_result = quote!{
                element.add_element({
                    let mut element = rustgular::VElement::create(#name, false, #xml_namespace, None);

                    #function_content

                    #viewchild_callback
                    
                    #render_content

                    element
                });
            };

            if let Some(pattern) = if_pattern {
                internal_result = quote!{
                    if #pattern {
                        #internal_result
                    }
                }
            }

            tokens.extend(internal_result);



        } else {
            // Recursively render DOM
            tokens.extend(render_content);  
        }
        

    }
}

impl Template {

    /// Generate a new root template
    pub fn new(content: Vec<dom_analysis::Node>, component_name: Ident, looked_for_view_childs: Vec<ViewChild>) -> Self {
        Self {
            template_type: TemplateType::Root,
            content,
            component_name,
            looked_for_view_childs
        }
    }

    /// Look for all HTML content (tags and text) and render content (recursively)
    fn render_inner_html(&self) -> TokenStream {

        let mut result = quote!();

        for node in &self.content {
            match node {
                dom_analysis::Node::StrNode(val) => {
                    let text = HtmlText(&val);
                    result.extend(quote!{
                        #text
                    });
                },
                dom_analysis::Node::Element(element) => {
                    // Remove *for option before forwarding to child
                    let mut attributes = element.attributes.clone();
                    let for_op = attributes.remove("*for");

                    let var_op = attributes.remove("*var");

                
                    let inner_template = Template {
                        template_type: TemplateType::Tag { name: element.tag.clone(), options: attributes, viewchild_tag: element.viewchild_tag.clone()},
                        content: element.children.clone(),
                        component_name: self.component_name.clone(),
                        looked_for_view_childs: self.looked_for_view_childs.clone()
                    };
            
                    let var_def = if let Some(var_op) = var_op {
                        let content: Expr = parse_str(&var_op).expect(&format!("Var Option: could not render option content `{}`", &var_op));
                        quote!{#content;}
                    } else {
                        quote!{}
                    };

                    // Look for "for x in y" recursion in child
                    if let Some(for_op) = for_op {
                        
                        let for_op = Regex::new(r#"let\s+(.+)\s+in\s+(.+)"#).unwrap().captures(&for_op).expect(&format!("For Option: could not render option content `{}`", &for_op));
                        
                        let var_name: Expr = parse_str(&for_op[1]).expect(&format!("For Option Variable name: could not render option content `{}`", &for_op[1]));
                        let source_name: Expr = parse_str(&for_op[2]).expect(&format!("For Option List name: could not render option content `{}`", &for_op[2]));
                        result.extend(quote!{
                            #var_def
                            for #var_name in #source_name {
                                #inner_template
                            }
                        });
                    } else {
                        result.extend(quote!{
                            #var_def
                            #inner_template
                        });
                    };
                }
            }
        }

        result
    }

    /// Look for `*if` attribute and render display condition if any
    fn render_if_condition(&self, options: &mut HashMap<String, String>) -> TokenStream {

        if let Some(option) = options.remove("*if") {

            let expr: Expr = parse_str(&option).expect(&format!("*if option: {} can't be rendered as a rust expression", &option));
            quote! {
                if ! { #expr } {
                    element.set_attribute("style", "display: None;");
                }
            }   
        } else {quote!()}
    }

    fn render_if_pattern(&self, options: &mut HashMap<String, String>) -> Option<Expr> {
        options.remove("*if-pattern").map(|pattern| {
            parse_str(&pattern).expect(&format!("*if-pattern option: {} can't be rendered as a rust expression", &pattern))
        })
    }

    /// Check all attributes for bindings
    /// Returns
    /// * The resolution to be applied
    fn render_binded_options(&self, options: &mut HashMap<String, String>, is_component: bool) -> TokenStream {

        let mut options_setters = quote!();

        for (option_name, option_value) in options {

            let option_value = option_value.replace("'", "\"");

            let option_screening = Regex::new(r#"\[(.+?)\]"#).unwrap().captures(option_name);
            let (name, resolution) = match option_screening {

                // [innerHTML] binding
                Some(_) if option_name == "[innerHTML]" => {
                    let expr: Expr = parse_str(&option_value).expect(&format!("*Binded option value: {} can't be rendered as a rust expression", &option_value));
                    options_setters.extend(quote!{
                        element.force_inner_html({ #expr }.to_string());
                    });
                    continue;
                },
                // Any other binding
                Some(option_name) =>  {
                    let expr: Expr = parse_str(&option_value).expect(&format!("*Binded option value: {} can't be rendered as a rust expression", &option_value));

                    // Special case for single class binding
                    if let Some(captures) = Regex::new(r#"class\.([\w-]+)"#).unwrap().captures(option_name.get(1).unwrap().as_str()) {
                        let class_name = captures.get(1).unwrap().as_str();
                        options_setters.extend(quote!{
                            if { #expr } {
                                element.classes.push(#class_name.to_string());
                            }

                        });
                        continue;
                    }

                    (option_name.get(1).unwrap().as_str(), quote!{{ #expr }})

                },
                // Not a [x] parameter => should be copied as is
                _ => {
                    if option_name == "class" {
                        let class_name = option_value;
                        options_setters.extend(quote!{
                            element.classes.push(#class_name.to_string());
                        });
                        continue;
                    }

                    let value: &str = option_value.as_str();
                    (&option_name[..], quote!{#value})
                }
            };

            // Check if it's a component input
            // If it's a component input => could by any type
            // Otherwise, must be rendered as String
            if is_component {
                let expr_name: Expr = parse_str(name).expect(&format!("Binded option key: `{}` can't be rendered as a rust expression", name));

                options_setters.extend(quote!{
                    child_struct.app.try_lock().unwrap().#expr_name(#resolution.into());
                });
    
            } 
            else {
                options_setters.extend(quote!{
                    element.set_attribute(#name, &#resolution.to_string());
                });
            }

        }
        options_setters
    }

    /// Create child component
    /// # Parameters:
    /// * tag_name: component name
    /// * child_setters: previously created setters for child inputs
    fn render_child_component(&self, tag_name: String, options: &mut HashMap<String, String>) -> TokenStream {
        
        // Look for generic arguments
        let generics = options.remove("*generics").map_or(quote!{}, |gen| {
            let temp: Expr = parse_str(&gen).expect("Couldn't parse generic arguments");
            quote!{<#temp>}
        });

        let class_name: Expr = parse_str(&tag_name[4..]).unwrap();
        let callbacks = self.render_callbacks(options, true);
        let child_setters = self.render_binded_options(options, true);

        quote!{

            // Check for children reuse
            let (mut boxed_child_struct, is_new) = previous_children[reuse_cursor..].iter().position(|child| child.get_tag() == #tag_name).map_or_else(
                || {
                    let child_struct = rustgular::Component::<app_module::#class_name #generics, app_module::Services>::new(
                        self.__rustgular_services.clone(),
                        self.__rustgular_render_trigger.clone()
                    );  
                    let mut boxed_child_struct: Box<dyn rustgular::ComponentRender<VElem = rustgular::VElement, S=app_module::Services>> = Box::new(child_struct);
                    (boxed_child_struct, true)
                },
                |index| {
                    let result = (previous_children.remove(reuse_cursor + index), false);
                    reuse_cursor = index;
                    result
                }
            );

            element.component_is_reused = is_new;
            let child_struct = boxed_child_struct.as_mut().as_mut_any().downcast_mut::<rustgular::Component::<app_module::#class_name #generics, app_module::Services>>().expect("Can't cast boxed child to component");

            #callbacks
            #child_setters

            child_struct.render(&mut element).await;
            children.push(boxed_child_struct);
        }
    }

    /// Look for callbacks inside `(xxx)` options and generate corresponding code
    /// Handle callback arguments must be `'static`
    fn render_callbacks(&self, options: &mut HashMap<String, String>, is_component: bool) -> TokenStream {
        // Setup callbacks
        let mut result = quote!();

        options.retain(|key, value| {            
            if &key[..1] == "(" && &key[key.len()-1..] == ")" {

                let callback_type = &key[1..key.len()-1];

                let callback_content: Expr = parse_str(value).unwrap();
                // let struct_name: Expr = parse_str(struct_name).unwrap();

                if is_component {
                    let callback_info = Regex::new("(.+?):(.+)").unwrap().captures(callback_type).expect("Callbacks to components should be structured as `x:T`, x specifying the event name and T the type");
                    
                    // Replace all « and » tokens
                    let type_str = callback_info[2].replace("«", "<").replace("»", ">");
                    let observer: Expr = parse_str(&callback_info[1]).unwrap();
                    let observer_type: Type = parse_str(&type_str).unwrap();

                    let callback_wrapper = callback_wrapper::CallbackWrapper{
                        content: &callback_content, 
                        argument_type: &observer_type,
                    };
                    result.extend(quote!{
                        // Only subscribe to component events if new component. Otherwise, already done previously
                        
                        // In any case, observers must be updated (because captured data may have changed)
                        let mut child_ref = child_struct.app.lock().await;
                        child_ref.#observer.clear();
                        child_ref.#observer.subscribe(
                            (
                                this.clone(), 
                                #callback_wrapper
                            )
                        );
                        drop(child_ref);

                    });
                    
                } else {

                    let callback_wrapper = callback_wrapper::CallbackWrapper {
                        content: &callback_content,
                        argument_type: &quote!{rustgular::web_sys::Event},
                    };
                    result.extend(quote!{
                        element.record_callback(#callback_type, rustgular::CallbackPointer(this.clone(), #callback_wrapper));
                    });
                }
                false
            } else { true }
        });
        result
    }

    /// Look for a namespace in options (`xmlns` option). If found, remove it from options and return it
    fn get_namespace(&self, options: &mut HashMap<String, String>) -> Option<String> {
        options.remove("xmlns")
    }
}