
use syn::{Field, Attribute, Expr, Lit};

/// If a specific attribute is attached to a field, remove it and return it
pub fn take_attr<'a>(field: &'a mut Field, attr_name: &str) -> Option<Attribute> {
    
    let index = field.attrs.iter().position(|attr| attr.meta.path().segments.last().unwrap().ident.to_string() == attr_name);

    index.map(|i| field.attrs.remove(i))

}

/// Turn a Syn `Expr` to its `String` value
/// Panic if the Expr doesn't reprensent a string litera 
pub fn expr_to_string(lit: &Expr) -> String {
    match lit {
        Expr::Lit(val) => {
            match &val.lit {
                Lit::Str(val) => Some(val.value()),
                _ => None
            } 
        }
        _ => None
    }.expect("Should be able to turn template to string")
}