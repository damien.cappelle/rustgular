use quote::{ToTokens, quote};
use syn::{Expr, parse_str};
use regex::Regex;

/// Represent brute text inside HTML (every piece of text not inside tags)
pub struct HtmlText<'a>(pub &'a str);

impl<'a> ToTokens for HtmlText<'a> {
    fn to_tokens(&self, tokens: &mut proc_macro2::TokenStream) {

        let mut arguments = quote!();

        for captures in Regex::new(r#"\{\{(.*?)\}\}"#).unwrap().captures_iter(self.0) {
            let arg: &str = captures.get(1).unwrap().into();
            let expr: Expr = parse_str(arg).unwrap();
            
            arguments.extend(quote!{
                ,{ #expr }.to_string ()
            });
        }
        let template: &str = &Regex::new(r#"\{\{(.*?)\}\}"#).unwrap().replace_all(self.0, "{}");
    
        if arguments.is_empty() {
            tokens.extend(quote!{
                element.append_with_str(#template);
            });
        } else {
            tokens.extend(quote!{
                element.append_with_str(&format!(#template #arguments));
            });
        }
           
    }
}