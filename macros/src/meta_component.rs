
use syn::{Type, PathArguments, TypePath, parse, Generics, GenericParam};
use syn::{Token, Visibility, token::Pub};
use syn::punctuated::Punctuated;
use syn::{Expr, Ident, DeriveInput, Fields, Field, token::Comma, LitStr, Lit};
use proc_macro2::TokenStream;
use proc_macro::TokenStream as TokenStream1;
use quote::quote;

use crate::dom_analysis;
use crate::on_init::OnInit;
use crate::tools::take_attr;
use crate::setter::Setter;
use crate::view_child::ViewChild;
use crate::template::Template;
use crate::window_callback::WindowCallback;

/// Contains all Meta data of a component
pub struct MetaComponent {
    /// Identity of the component
    ident: Ident,
    /// Possible generics
    generics: Generics,
    /// Fields expression
    fields: Punctuated<Field, Comma>,
    /// Setters to be rendered
    setters: Vec<Setter>,
    /// HTML Template
    template: Template,
    /// Window callbacks provided by component
    window_callbacks: Vec<WindowCallback>,
    /// Component can impersonate any tag name it wants.
    impersonate: Option<LitStr>,
    /// Possible callback to execute after the first rendering
    on_init: Option<Expr>,
    /// Style defined for this component only
    style: Option<Expr>
}


impl MetaComponent {
    /// Generate new MetaComponent from identity, template and styles
    pub fn new(item: TokenStream, template: String, style: Option<&Expr>, window_callbacks: Option<&Expr>, impersonate: Option<&Expr>, on_init: Option<&Expr>) -> Self {

        // Extract structure name
        let item: TokenStream1 = item.into();
        // let content: DeriveInput = parse_macro_input!(item as DeriveInput)/
        let content = syn::parse::<DeriveInput>(item).expect("Should be able to parse structure as DeriveInput");

        let ident = content.ident.clone();
        let generics = content.generics.clone();

        let nodes = dom_analysis::analyse_structure(&template);

        let template = Template::new(nodes, ident.clone(), vec![]);

        let window_callbacks = WindowCallback::from_list_expr(window_callbacks);

        match content.data {
            syn::Data::Struct(content) => {
                match content.fields {
                    Fields::Named(fields) => {
                        let mut out = Self {
                            ident,
                            generics,
                            fields: fields.named,
                            setters: vec![],
                            template,
                            window_callbacks,
                            impersonate: impersonate.map(|im| syn::parse::<LitStr>(quote!{#im}.into()).expect("Can't turn impersonate option to string literal")),
                            on_init: on_init.cloned(),
                            style: style.cloned()
                        };
                        out.analyse_fields();
                        out
                    }
                    _ => panic!("Fields should be named")
                }
            },
            _ => panic!("component only applyable on structures")
        }

    }

    /// Generate final Component code
    pub fn generate(&self) -> TokenStream {

        let ident = &self.ident;
        let generics_with_bounds = &self.generics;
        let mut generics_without_bounds = self.generics.clone();
        for param in &mut generics_without_bounds.params {
            match param {
                GenericParam::Type(param) => {param.bounds.clear()},
                _ => {}
            }
        };

        let selector = format!("app-{}", ident.to_string());

        let fields = &self.fields;
        let setters = &self.setters;
        let template = &self.template;

        let default_setters: Vec<TokenStream> = fields.iter().map(|field| {
            let mut field = field.clone();
            let ident = &field.ident;
            let ty = &mut field.ty;

            match ty {
                Type::Path(ty) => {
                    let path = &mut ty.path;
                    let seg = path.segments.last_mut().unwrap();
                    let backup_args = seg.arguments.clone();
                    seg.arguments = PathArguments::None;

                    let bracket = if let PathArguments::AngleBracketed(args) = backup_args {
                        quote!{::#args}
                    } else {
                        quote!{}
                    };

                    quote!{
                        #ident: #ty #bracket::default(), 
                    }

                },
                Type::Tuple(ty) => {
                    quote!{
                        #ident: <#ty>::default(), 
                    }
                }
                _ => panic!("Can't render type {:?}", ty)
            }


        }
        ).collect();

        let window_callbacks = &self.window_callbacks;

        let impersonate_function = if let Some(impersonate) = &self.impersonate {
            quote!{
                fn get_impersonation() -> Option<&'static str> {
                    Some(#impersonate)
                }
            }
        } else {
            quote!{}
        };

        let on_init = OnInit{content: &self.on_init};

        let apply_style = self.style.as_ref().map(|style|
            quote!{
                element.style = Some(#style.to_string());
            }
        ).unwrap_or(quote!{});

        quote!{
            use std::fmt::Display;
    
            pub struct #ident #generics_with_bounds {
                should_be_rendered: bool,
                __rustgular_services: rustgular::Arc<rustgular::Mutex<app_module::Services>>,
                __rustgular_render_trigger: rustgular::async_channel::Sender::<()>,
                #fields
            }
    
            impl #generics_with_bounds #ident #generics_without_bounds {
                #(#setters)*
            }
    
            #[rustgular::async_trait(?Send)]
            impl #generics_with_bounds rustgular::External<app_module::Services> for #ident #generics_without_bounds {
                fn new(services: rustgular::Arc<rustgular::Mutex<app_module::Services>>, trigger: rustgular::async_channel::Sender::<()>) -> Self {
                    Self{
                        should_be_rendered: true,
                        __rustgular_services: services,
                        __rustgular_render_trigger: trigger,
                        #(#default_setters)*
                    }
                }
            
                fn request_rendering(&mut self) {
                    self.should_be_rendered = true;
                }
            
                fn should_be_rerendered(&mut self) -> bool {
                    let rsp = self.should_be_rendered;
                    self.should_be_rendered = false;
                    rsp
                }
                
                fn install_window_callbacks(&self, this: rustgular::Arc<rustgular::Mutex<Self>>, root: &mut rustgular::VElement) {
                    #(#window_callbacks)*
                }

                async fn render(&self, this: rustgular::Arc<rustgular::Mutex<Self>>, services: rustgular::Arc<rustgular::Mutex<app_module::Services>>, element: &mut rustgular::VElement, children: &mut Vec<Box<dyn rustgular::ComponentRender<VElem = rustgular::VElement, S=app_module::Services>>>) {

                    #apply_style

                    self.install_window_callbacks(this.clone(), element);
                    let mut previous_children: Vec<Box<dyn rustgular::ComponentRender<VElem = rustgular::VElement, S=app_module::Services>>> = vec![];

                    // Move all existing children to an archive
                    for i in 0..children.len() {
                        previous_children.push(children.remove(0))
                    }
                    let mut reuse_cursor = 0;
                    #template
                }

                fn on_init(this: rustgular::Arc<rustgular::Mutex<Self>>) {
                    // Execute on_init if any
                    #on_init
                }

                fn get_tag() -> &'static str {
                    #selector
                }

                #impersonate_function
            }
        }

    }

    /// Analyse all fields looking for
    /// * #[child_view] identifying childviews
    /// * #[input] identifying fields for which a setter must be created
    /// * #[output] identifying fields for event fields (signals to parent)
    /// 
    /// When an attribute is found, it's removed
    /// 
    /// Add `should_be_rendered` field indicating that something changed
    fn analyse_fields(&mut self) {
        
        // Add `should_be_rendered field



        // self.fields.push(Field::parse_named(quote!(should_be_rendered: bool).into()).unwrap());

        // For all fields
        for field in self.fields.iter_mut() {
    
            let ident = field.ident.clone().unwrap();
    
            // Look for input attribute
            if take_attr(field, "input").is_some() {
                let field_type = field.ty.clone();

                // If found, add a setter
                self.setters.push(
                    Setter {
                        ident,
                        field_type
                    }
                );
            }
    
            // Look for output attribute
            if take_attr(field, "output").is_some() {
                let field_type = field.ty.clone();

                field.vis = Visibility::Public(Pub::default());

                // Change here Verbatim by Path 
                field.ty = Type::Path(
                    parse::<TypePath>(quote!{
                    rustgular::Observable<#field_type>
                }.into()).unwrap());
                // field.ty = Type::Verbatim(quote!{
                //     rustgular::Observable<#field_type>
                // })
            }

            // Look for view_child
            if let Some(view_child_attr) = take_attr(field, "view_child") {

                let params = view_child_attr.parse_args_with(Punctuated::<Expr, Token![,]>::parse_terminated).expect("Can't parse ViewChild parameters");

                let name = match params.first() {
                    Some(Expr::Lit(name)) => {
                        match &name.lit {
                            Lit::Str(name) => {
                                name.value()
                            },
                            _ => panic!("Viewchild first parameter must be a string corresponding to its name")
                        }
                    },
                    _ => panic!("Viewchild first parameter must be a string corresponding to its name")
                };

                // view child callbacks are optional
                let callback = if params.len() == 2 {
                    params.last()
                } else {
                    None
                };
                

                self.template.looked_for_view_childs.push(ViewChild{
                    field: field.ident.clone().expect("`view_child` field must be a named field"), 
                    name,
                    callback: callback.cloned()
                });
            }
        };
    
    }

}
