
use std::collections::HashMap;

use regex::{Regex, RegexBuilder};

#[derive(Debug, PartialEq, Clone)]
pub enum Node {
    StrNode(String),
    Element(Element)
}

#[derive(Debug, PartialEq, Default, Clone)]
pub struct Element {
    pub tag: String,
    pub children: Vec<Node>,
    pub attributes: HashMap<String, String>,
    pub viewchild_tag: Option<String>
}

/// Analyse a XML template and return the corresponding structure
pub fn analyse_structure(template: &str) -> Vec<Node> {
    let mut result = vec![];


    let mut analyse_cursor = 0;

    // Look for html tags
    let html_tag_regex = RegexBuilder::new(r#"\s*<([\w-]+)(.*?)>\s*"#).dot_matches_new_line(true).build().unwrap();
    while let Some(captures) = html_tag_regex.captures(&template[analyse_cursor..]) {
        
        let mut element = Element::default();

        let template = &template[analyse_cursor..];
    
        // Represent the complete starting tag 
        let entire_match = captures.get(0).unwrap();            
    
        // If something was before the found tag, record it
        if !template[..entire_match.start()].is_empty() {
            result.push(Node::StrNode(template[..entire_match.start()].to_string()));
        } 

        let tag_name: &str = captures.get(1).unwrap().into();
        element.tag = tag_name.to_string();

        // Rebase template at match start and look for corresponding closing tag
        let template = &template[entire_match.start()..];
        let (closing_tag_start, closing_tag_end, self_closed) = find_corresponding_closing_tag(template);
    
        let child_options_str: &str = captures.get(2).unwrap().into();

        for op in RegexBuilder::new(r#"([:&\(\)\.\[\]\*«»\w-]+?)="([^"]+)""#).dot_matches_new_line(true).build().unwrap().captures_iter(child_options_str) {
            element.attributes.insert(op[1].to_string(), op[2].to_string());
        }

        element.viewchild_tag = Regex::new(r#"#(\w+)"#).unwrap().captures(child_options_str).map(
            |captures| captures[1].to_string()
        );

        // If it's not an auto closed element, check inside
        if !self_closed {
            element.children.append(&mut analyse_structure(&template[(entire_match.end()-entire_match.start())..closing_tag_start]));
        }

        result.push(Node::Element(element));

        // Increment analyse_cursor to be at end of analysed tag
        analyse_cursor += closing_tag_end + entire_match.start();
    }

    // If there is any remaining stuff after last tag, record it
    if !template[analyse_cursor..].is_empty() {
        result.push(Node::StrNode(template[analyse_cursor..].to_string()));
    } 

    result

}

/// Seach for closing tag in an xml template corresponding to first tag and returns start of ending tag index, end of ending tag index
/// and whether it's a self closed tag
/// 
/// Template is expected to start with a tag
/// 
/// # Example
/// ```
/// use crate::dom_analysis::*;
/// let closing_tag_position = find_corresponding_closing_tag("<div> <div><a></a></div> </div> <div> </div></div>");
/// assert_eq!(31, closing_tag_position);
/// ```
/// # Panic
/// When no closing tag is found before end of template or if the template doesn't start with a opening tag
/// 
pub fn find_corresponding_closing_tag(template: &str) -> (usize, usize, bool) {
    // Identify first tag
    let captures = &RegexBuilder::new(r#"<\s*([\w-]+).*?(/?)\s*>"#).dot_matches_new_line(true).build().unwrap()
        .captures(template).expect("Template should start with an opening tag");

    if !captures[2].is_empty() {
        return (captures.get(0).unwrap().start(), captures.get(0).unwrap().end(), true)
    }

    let opening_tag = &captures[1];
    let mut tag_counter = 0;
    for captures in Regex::new(&format!(r#"<\s*(/?)\s*{}\W"#, opening_tag)).unwrap().captures_iter(template) {
        if captures[1].is_empty() {
            tag_counter += 1;
        }
        else {
            tag_counter -= 1;
        }

        if tag_counter == 0 {
            return (captures.get(0).unwrap().start(), captures.get(0).unwrap().end(), false);
        }
    };

    panic!("Couldn't find closing tag");
}


#[cfg(test)]
mod tests {

    use super::*;

    #[test]
    fn test_existing_closing_tag() {
        let closing_tag_position = find_corresponding_closing_tag("<div> <div><br/> <a></a></div> </div><div> </div></div>");
        assert_eq!((31, 37, false), closing_tag_position);
    }

    #[test]
    fn test_handle_auto_closed_tag() {
        let closing_tag_position = find_corresponding_closing_tag("<br/><div><a></a></div>");
        assert_eq!((0, 5, true), closing_tag_position);
    }

    #[test]
    fn no_existing_closing_tag() {

    }

    #[test]
    fn generate_populating_function() {

        // let template = r#"<div><p *for="let a in list_of_a"><br/><p>Test</p></p><p>Super info</p><div *for="let b in list_of_b"><div *for="let c in b"><a href="www.super.link"> Oh {{ c }}</a></div></div></div>"#;

        // let result = analyse_template(None, "", template, "super_struct");

        // assert_eq!(quote!{
        //         element.append_with_node({
        //             let element = document.create_element("div").unwrap();
        //             for a in list_of_a {
        //                 element.append_with_node({
        //                     let element = document.create_element("p").unwrap();
        //                     element.append_with_node({
        //                         let element = document.create_element("br").unwrap();
        //                         element
        //                     });
        //                     element.append_with_node({
        //                         let element = document.create_element("p").unwrap();
        //                         element.set_inner_html("Test");
        //                         element
        //                     });
        //                     element
        //                 });
        //             }
        //             element.append_with_node({
        //                 let element = document.create_element("p").unwrap();
        //                 element.set_inner_html("Super info");
        //                 element    
        //             });
        //             for b in list_of_b {
        //                 element.append_with_node({
        //                     let element = document.create_element("div").unwrap();
        //                     for c in b {
        //                         element.append_with_node({
        //                             let element = document.create_element("div").unwrap();
        //                             element.append_with_node({
        //                                 let element = document.create_element("a").unwrap();
        //                                 element.set_attribute("href", "www.super.link");
        //                                 element.set_inner_html(&format!(" Oh {}", &{c}.to_string()));
        //                                 element
        //                             });
        //                             element
        //                         });
        //                     }    
        //                     element
        //                 });
        //             }
        //             element
        //         });
                
                
        // }.to_string(), result.to_string());
    }

}