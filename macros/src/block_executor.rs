
use proc_macro2::TokenStream;
use quote::{quote, ToTokens};
use syn::Expr;

pub struct BlockExecutor<'a> {
    pub content: &'a Expr,
    pub arc_name: &'a Expr
}

impl<'a> ToTokens for BlockExecutor<'a> {

    fn to_tokens(&self, tokens: &mut TokenStream) {
        let content = self.content;
        let arc_name = self.arc_name;
        let is_async = if let Expr::Async(_) = content {
            true
        } else {
            false
        };

        let this_and_content = quote!{

            let mut t = t.deref_mut();

            let this = t.as_mut_any().downcast_mut::<Self>().expect("Should be able to get app mutable reference from mutex");
            // let this = Self::from_dyn_callbackable(t);
            #content
        };

        tokens.extend(if is_async {
            quote!{
                // let this = t.clone();
                rustgular::spawn_local(async move {
                    let t = #arc_name.clone();
                    let mut t = t.lock().await;
                    #this_and_content.await;
                    
                    this.__rustgular_render_trigger.try_send(()).expect("Should be able to require rendering");    
                });
            }
        } else {
            quote!{
                match #arc_name.try_lock() {
                    Some(mut t) => {
                        #this_and_content;
                        this.__rustgular_render_trigger.try_send(()).expect("Should be able to require rendering");  
                    },
                    None => {
                        rustgular::console_error(&format!("Could not lock component to execute callback for {}", file!()));
                    }
                };
            }
        });
    }
}
