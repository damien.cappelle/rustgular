
use quote::{ToTokens, quote};
use syn::{Ident, Expr, parse_quote};

#[derive(Clone, Debug)]
pub struct ViewChild {
    pub field: Ident,
    pub name: String,
    pub callback: Option<Expr>
}

impl ToTokens for ViewChild {
    fn to_tokens(&self, tokens: &mut proc_macro2::TokenStream) {


        let var_name = &self.field;
        let callback = self.callback.as_ref().map(|cb| {
            if let Expr::Async(_) = cb {
                quote!{async {
                   this.#var_name = Some(event.unchecked_into());
                    #cb.await
                }
            }
            } else {
                // Certainly a bug here ! => To be handled when this happen
                quote!{
                   this.#var_name = Some(event.unchecked_into());
                    #cb
                }
            }
        }).unwrap_or(quote!{
            this.#var_name = Some(event.unchecked_into())
        });
        
        let callback_wrapper = crate::callback_wrapper::CallbackWrapper {
            content: &parse_quote!(#callback),
            argument_type: &quote!{rustgular::web_sys::Element}
        };

        tokens.extend(
        quote!{
            element.viewchild_callback = Some(rustgular::CallbackPointer(this.clone(), #callback_wrapper));
        })
    }
}