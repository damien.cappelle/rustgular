
use syn::Expr;
use quote::{quote, ToTokens};

use crate::block_executor::BlockExecutor;


pub struct OnInit<'a> {
    pub content: &'a Option<Expr>
}

impl<'a> ToTokens for OnInit<'a> {
    fn to_tokens(&self, tokens: &mut proc_macro2::TokenStream) {

        match self.content {
            None => {},
            Some(content) => {
                let block = BlockExecutor{
                    content: content,
                    arc_name: &Expr::Verbatim(quote!{this})
                };
                block.to_tokens(tokens);
            }
        }
    }
}