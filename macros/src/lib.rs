
mod dom_analysis;
mod meta_component;
mod setter;
mod view_child;
mod tools;
mod template;
mod html_text;
mod window_callback;
mod callback_wrapper;
mod block_executor;
mod on_init;
extern crate proc_macro;

use proc_macro::TokenStream;
use syn::{parse_macro_input, FieldValue, punctuated::Punctuated, Token};
use syn::{Member, Expr};
use tools::expr_to_string;
use std::fs::File;
use std::io::Read;

#[proc_macro_attribute]
pub fn component(attr: TokenStream, item: TokenStream) -> TokenStream {
    
    // Retrieve macro parameters
    let  parameters = parse_macro_input!(attr with Punctuated::<FieldValue, Token![,]>::parse_terminated);

    // Extract template
    let template = find_parameter(&parameters, "template").map(|temp| {
        expr_to_string(temp)
    }).or_else(|| {
        find_parameter(&parameters, "template_url").map(|url| {
            let url = expr_to_string(url);
            let mut file = File::open(url).expect("Template url doesn't exist");
            let mut content = String::new();
            file.read_to_string(&mut content).expect("Can't read template as String");
            content
        })
    }).expect("Can't find any parameter `template` or `template_url` in Component input parameters").clone();

    // Extract styles
    let styles = find_parameter(&parameters, "styles");

    // Extract Window-level callbacks
    let window_callbacks = find_parameter(&parameters, "window_callbacks");

    // Extract impersonation if any
    let impersonate = find_parameter(&parameters, "impersonate");

    let on_init = find_parameter(&parameters, "on_init");

    // Compute Meta Component based on template and styles and generate it

    meta_component::MetaComponent::new(item.into(), template, styles, window_callbacks, impersonate, on_init).generate().into()

}

/// Search and return a specific parameter from a Punctuated syn list
/// 
/// # Arguments
/// * `parameters`: Punctuated list
/// * `item`: name of the parameter
/// 
/// # Returns
/// * `Some(&Expr)` containing the Expr contained if found
/// * `None` otherwise
fn find_parameter<'a>(parameters: &'a Punctuated<FieldValue, Token![,]>, item: &str) -> Option<&'a Expr> {
    parameters.into_iter().find(|param| {
        if let Member::Named(param_name) = &param.member {
            &param_name.to_string() == item
        } else {false}
    }).map(|elem| &elem.expr)
}