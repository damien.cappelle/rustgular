
use syn::{Expr, LitStr};
use quote::{quote, ToTokens};
pub struct WindowCallback {
    name: LitStr,
    executor: Expr
}

impl WindowCallback {
    pub fn from_list_expr(callbacks: Option<&Expr>) -> Vec<Self> {
        // Extract window callbacks
        callbacks.map_or(vec![],|wc| {
            match wc {
                Expr::Array(wc) => {
                    wc.elems.iter().map(|wc| {
                        match wc {
                            Expr::Tuple(wc) => {
                                let name = wc.elems.first().expect("Window callback tuple should have 2 elements");
                                Self {
                                    name: syn::parse::<LitStr>(quote!{#name}.into()).expect("Window callback type should be string"),
                                    executor: wc.elems.last().expect("Window callback tuple should have 2 elements").clone()
                                }
                            },
                            _ => panic!("Each window callback should be a tuple")
                        }
                    }).collect()
                },
                _ => panic!("Window callbacks should be presented as array !")
            }
        })
    }
}

impl ToTokens for WindowCallback {
    fn to_tokens(&self, tokens: &mut proc_macro2::TokenStream) {

        let callback_type = &self.name;
        let executor = &self.executor;


        let callback_pointer = crate::callback_wrapper::CallbackWrapper {
            content: &executor.clone(),
            argument_type: &quote!{rustgular::web_sys::Event},
        };

        tokens.extend(quote!{
            root.record_window_callback(#callback_type, rustgular::CallbackPointer(this.clone(), #callback_pointer));
        });           
    }
}