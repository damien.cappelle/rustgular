
use proc_macro2::TokenStream;
use syn::{Ident, Type};
use quote::{quote, ToTokens};

/// Represent a setter for a specific field
pub struct Setter {
    /// Field name
    pub ident: Ident,
    /// Field type
    pub field_type: Type
}

impl ToTokens for Setter {

    /// Generate a setter function code
    fn to_tokens(&self, tokens: &mut TokenStream) {
        let ident = &self.ident;
        let field_type = &self.field_type;

        tokens.extend([quote!{
            pub fn #ident(&mut self, value: #field_type) {
                self.#ident = value;
            }
        }]);
    }


}
