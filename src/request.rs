
use serde::{de::DeserializeOwned, Serialize};
use serde_yaml;
use web_sys::Response;
use wasm_bindgen::JsCast;

#[derive(Debug, Clone)]
pub struct Request {
    response: Response
}

impl Request {
    pub async fn post<T: Serialize>(url: &str, data: Option<&T>) -> Result<Self,()> {

        let mut init = web_sys::RequestInit::new();
        init.method("POST");
        if let Some(data) = data {
            init.body(Some(&wasm_bindgen::JsValue::from_str(&serde_json::to_string(&data).expect("Couldn't convert data to json"))));
        }

        let request = web_sys::Request::new_with_str_and_init(url, &init).expect("Couldn't generate request");
        request.headers().set("content-type", "application/json").unwrap();

        let res = wasm_bindgen_futures::JsFuture::from(web_sys::window().unwrap().fetch_with_request(&request)).await.or(Err(()))?;
        let resp: web_sys::Response = res.dyn_into().unwrap();

        Ok(Self{
            response: resp
        })
    }

    pub async fn get(url: &str) -> Result<Self,()> {
        let res = wasm_bindgen_futures::JsFuture::from(web_sys::window().unwrap().fetch_with_str(url)).await.or(Err(()))?;
        let resp: web_sys::Response = res.dyn_into().unwrap();

        Ok(Self{
            response: resp
        })
    }

    pub async fn as_yml<T: DeserializeOwned>(&self) -> Result<T, ()> {

        let resp = wasm_bindgen_futures::JsFuture::from(self.response.text().unwrap()).await.or(Err(()))?;
        let resp = resp.as_string().ok_or(())?;

        let rsp: T = serde_yaml::from_str(&resp).or(Err(()))?;

        Ok(rsp)
    }

    pub async fn as_json<T: DeserializeOwned>(&self) -> Result<T, ()> {
        let resp = wasm_bindgen_futures::JsFuture::from(self.response.text().unwrap()).await.or(Err(()))?;
        let resp = resp.as_string().ok_or(())?;

        let rsp: T = serde_json::from_str(&resp).or(Err(()))?;

        Ok(rsp)

    }
}



