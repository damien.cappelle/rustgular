
pub mod velement;
pub mod vnode;

#[cfg(test)]
mod tests {

    use super::{velement::*, vnode::*};
    use web_sys;
    use async_channel;
    pub use async_std::{sync::{Arc, Mutex}, self};

    use wasm_bindgen_test::*;
    wasm_bindgen_test::wasm_bindgen_test_configure!(run_in_browser);

    use wasm_bindgen::prelude::*;

    #[wasm_bindgen_test]
    async fn can_apply_and_reapply_virtual_dom() {
        

        // use std::panic;
        // panic::set_hook(Box::new(rustgular::console_error_panic_hook::hook));

        let document = web_sys::window().unwrap().document().unwrap();
        document.get_elements_by_tag_name("body").item(0).unwrap().append_child(document.create_element("app-component").unwrap().as_ref()).unwrap();

        // Look for entrypoint
        let entrypoints = web_sys::window().unwrap().document().unwrap().get_elements_by_tag_name("app-component");
        let mut entrypoint = entrypoints.item(0).unwrap();
        entrypoint.set_inner_html("");
        let (s, _): (async_channel::Sender::<()>, async_channel::Receiver::<()>) = async_channel::unbounded();
        

        let mut root_velem = VElement::create("root", false, None, None);

        root_velem.append_with_str("Test");
        root_velem.add_element(VElement::create("div", false, None, None));
        let mut sub_root_velem = VElement::create("subroot", false, None, None);
        sub_root_velem.add_element(VElement::create("p", false, None, None));
        root_velem.add_element(sub_root_velem);

        // Empty virtual DOM root
        let vnode = VNode{
            node: VNodeType::Element(root_velem),
            hash_result: None
        };


        let vnode = vnode.apply_to_dom(None, &mut entrypoint, s.clone(), &None);

        let child_nodes = entrypoint.child_nodes();
        // Check DOM structure
        assert_eq!(child_nodes.length(), 3);

        assert_eq!(child_nodes.item(0).unwrap().node_value().unwrap(), "Test".to_string());

        let node1: &web_sys::HtmlElement = &child_nodes.item(1).unwrap().unchecked_into();
        let node2: &web_sys::HtmlElement = &child_nodes.item(2).unwrap().unchecked_into();
        let node20: &web_sys::HtmlElement = &node2.child_nodes().item(0).unwrap().unchecked_into();

        assert_eq!(node1.tag_name(), "DIV".to_string());
        assert_eq!(node2.tag_name(), "SUBROOT".to_string());
        assert_eq!(node20.tag_name(), "P".to_string());

        // Try to re-render
        let prev = vnode.clone();
        let vnode = vnode.apply_to_dom(Some(prev), &mut entrypoint, s.clone(), &None);

        let child_nodes = entrypoint.child_nodes();
        // Check DOM structure
        assert_eq!(child_nodes.length(), 3);

        assert_eq!(child_nodes.item(0).unwrap().node_value().unwrap(), "Test".to_string());

        let node1: &web_sys::HtmlElement = &child_nodes.item(1).unwrap().unchecked_into();
        let node2: &web_sys::HtmlElement = &child_nodes.item(2).unwrap().unchecked_into();
        let node20: &web_sys::HtmlElement = &node2.child_nodes().item(0).unwrap().unchecked_into();

        assert_eq!(node1.tag_name(), "DIV".to_string());
        assert_eq!(node2.tag_name(), "SUBROOT".to_string());
        assert_eq!(node20.tag_name(), "P".to_string());


        // Update an attribute to a node

        let prev = vnode;

        let mut root_velem = VElement::create("root", false, None, None);
        root_velem.append_with_str("Test");
        root_velem.add_element(VElement::create("div", false, None, None));
        let mut sub_root_velem = VElement::create("subroot", false, None, None);
        sub_root_velem.add_element(VElement::create("p", false, None, None));
        sub_root_velem.set_attribute("attr", "cool-attr");
        root_velem.add_element(sub_root_velem);

        // Empty virtual DOM root
        let vnode = VNode{
            node: VNodeType::Element(root_velem),
            hash_result: None
        };

        vnode.apply_to_dom(Some(prev), &mut entrypoint, s.clone(), &None);

        let child_nodes = entrypoint.child_nodes();
        // Check DOM structure
        assert_eq!(child_nodes.length(), 3);

        assert_eq!(child_nodes.item(0).unwrap().node_value().unwrap(), "Test".to_string());

        let node1: &web_sys::HtmlElement = &child_nodes.item(1).unwrap().unchecked_into();
        let node2: &web_sys::HtmlElement = &child_nodes.item(2).unwrap().unchecked_into();
        let node20: &web_sys::HtmlElement = &node2.child_nodes().item(0).unwrap().unchecked_into();

        assert_eq!(node1.tag_name(), "DIV".to_string());
        assert_eq!(node2.tag_name(), "SUBROOT".to_string());
        assert_eq!(node2.get_attribute("attr"), Some("cool-attr".to_string()));
        assert_eq!(node20.tag_name(), "P".to_string());
        
   
    }

    #[wasm_bindgen_test]
    async fn can_apply_classes() {
    
        let document = web_sys::window().unwrap().document().unwrap();
        document.get_elements_by_tag_name("body").item(0).unwrap().append_child(document.create_element("app-component").unwrap().as_ref()).unwrap();

        // Look for entrypoint
        let entrypoints = web_sys::window().unwrap().document().unwrap().get_elements_by_tag_name("app-component");

        let (s, _): (async_channel::Sender::<()>, async_channel::Receiver::<()>) = async_channel::unbounded();
        
        let mut root_velem = VElement::create("root", false, None, None);

        let mut child_elem = VElement::create("div", false, None, None);
        child_elem.classes = vec!["test1".to_string(), "test2".to_string()];
        root_velem.add_element(child_elem);

        // Empty virtual DOM root
        let vnode = VNode{
            node: VNodeType::Element(root_velem),
            hash_result: None
        };

        let mut entrypoint = entrypoints.item(0).unwrap();

        vnode.apply_to_dom(None, &mut entrypoint, s.clone(), &None);

        let child_nodes = entrypoint.child_nodes();
        // Check DOM structure
        let node1: &web_sys::HtmlElement = &child_nodes.item(0).unwrap().unchecked_into();
        assert_eq!(node1.tag_name(), "DIV".to_string());
        assert_eq!(node1.class_name(), "test1 test2".to_string());
   
    }

}