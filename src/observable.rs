
use crate::Callbackable;
use std::rc::Rc;
use async_std::sync::{Arc, Mutex};

pub type CallbackPointer<T> = (Arc<Mutex<dyn Callbackable>>, Rc<dyn Fn(Arc<Mutex<dyn Callbackable>>, &T) -> ()>);

/// Subcribable object that call registered function when value is changed
pub struct Observable<T> {
    /// List of callbacks to call when value is updated
    observers: Vec<CallbackPointer<T>>
}

impl<T> Default for Observable<T> {
    fn default() -> Self {
        Self { observers: vec![] }
    }
}

impl<T> Observable<T> {

    /// Create a new observer with a default value
    pub fn new() -> Self {
        Self {
            observers: vec![] 
        }
    }

    /// Update observer value
    /// This automatically call registered functions
    pub fn set(&mut self, value: &T) {

        for observer in &self.observers {
            // let mut this = observer.0.try_lock().unwrap();
            // let t = this.deref_mut();
            observer.1(observer.0.clone(), value);
        }
    }

    /// Register a new subscription funciton
    pub fn subscribe(&mut self, callback: CallbackPointer<T>) {
        self.observers.push(callback);
    }

    pub fn clear(&mut self) {
        self.observers.clear();
    }

}

#[cfg(test)]
mod tests {

    use async_std::sync::{Arc, Mutex};
    use std::{rc::Rc, ops::Deref};
    struct Component {
        calls: Vec<usize>
    }
    

    use crate::{Observable, Callbackable};


    #[test]
    fn can_subscribe() {

        let this = Arc::from(Mutex::new(Component{ calls: vec![] }));
        let mut obs = Observable::<usize>::new();


        obs.subscribe((this.clone(), Rc::from(|_t: Arc<Mutex<dyn  Callbackable>>, _e: &usize| {

            // let t = t.as_mut_any().downcast_mut::<Component>().unwrap();
            // t.calls.push(*e);
        })));

        obs.set(&1);
        obs.set(&2);
        obs.set(&3);
        
        let binding = this.try_lock().unwrap();
        let this = binding.deref();

        
        assert_eq!(this.calls, [1,2, 3]);
    }

    #[test]
    fn can_clear_subscriptions() {

    }

}