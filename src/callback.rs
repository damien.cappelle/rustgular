
use web_sys::{Event, EventTarget};
use wasm_bindgen::prelude::*;
use js_sys::Function;

use crate::CallbackPointer;

#[derive(Debug)]
pub struct Callback {
    pub event_type: String,
    pub element: Option<EventTarget>,
    pub callback: CallbackPointer<web_sys::Event>,
    pub handler: Option<Closure<dyn Fn(Event)>>
}

impl Clone for Callback {
    fn clone(&self) -> Self {
        Self {
            event_type: self.event_type.clone(),
            element: None,
            callback: self.callback.clone(),
            handler: None
        }
    }
}

impl Callback {
    pub fn install(&mut self, element: &EventTarget, event_type: &str) {
        let this = self.callback.0.clone();
        let cb_ptr = self.callback.1.clone();

        let cb = Closure::<dyn Fn(Event)>::wrap(Box::new(move |event: Event| {

            let cb_ptr = cb_ptr.clone();
            // event.stop_propagation();

            let app = this.clone();
            (cb_ptr)(app, event);
        }));

        element.add_event_listener_with_callback(
            event_type, 
            cb.as_ref().unchecked_ref()
        ).expect("Couldn't add event listener");

        self.handler = Some(cb);

        let target: &EventTarget = element.as_ref();
        self.element.get_or_insert(target.clone());
    }
}


/// Implement drop to remove html callback when callback is deallocated
impl Drop for Callback {
    fn drop(&mut self) {
        if let Some(handler) = &self.handler {
            if let Some(element) = &self.element {
                let function: &Function  = handler.as_ref().dyn_ref().unwrap();
                element.remove_event_listener_with_callback(&self.event_type, function).expect("Can't remove callback");

            }
        }
    }
}