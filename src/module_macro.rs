#[macro_export]
macro_rules! module {
    ( $( $x:ident => $y:ty ),* ; $a:ty ) => {

        #[derive(Debug, Default)]
        pub struct Services {
            $(pub $x: $y,)*
        }        

        impl Services {
            pub fn new() -> Self {
                Self {
                    $(
                        $x: <$y>::default(),
                    )*
                }
            }
        }
        

        pub fn initialize_entrypoint() {

            // Look for entrypoint
            let entrypoints = rustgular::web_sys::window().unwrap().document().unwrap().get_elements_by_tag_name("app-component");

            if entrypoints.length() != 1 {
                panic!("There should be only 1 entrypoint element");
            }


            let (s, r): (rustgular::async_channel::Sender::<()>, rustgular::async_channel::Receiver::<()>) = rustgular::async_channel::unbounded();
            let services = rustgular::Arc::from(rustgular::Mutex::new(Services::new()));

            let mut root_component = rustgular::Component::<$a, Services>::new(services, s.clone());
            
            // Empty virtual DOM root
            let mut vnode = rustgular::virtual_dom::vnode::VNode{
                node: rustgular::virtual_dom::vnode::VNodeType::Element(rustgular::VElement::create("root", false, None, None)),
                hash_result: None
            };

            // let services_ref = services.try_lock().unwrap();
            // let services_ref = &services_ref.deref();
    


            // let services = services.clone();
            rustgular::spawn_local(async move {

                root_component.render(&mut vnode.unchecked_into_element()).await;    

                let mut backup_vnode = vnode.apply_to_dom(None, &mut entrypoints.item(0).unwrap(), s.clone(), &None);
                loop {
                    
                    
                    // use std::time::Duration;

                    // Wait for an render event issued by callbacks
                    // Regenerate virtual dom
                    r.recv().await.expect("Rendering channel should never be closed");

                    // let services_guard = services.try_lock().unwrap();
                    // let services_ref = &services_guard.deref();
        
                    let mut vnode = rustgular::virtual_dom::vnode::VNode{
                        node: rustgular::virtual_dom::vnode::VNodeType::Element(rustgular::VElement::create("root", false, None, None)),
                        hash_result: None
                    };
        
                    root_component.render(&mut vnode.unchecked_into_element()).await;
                    // drop(services_guard);
                    
                    // Update real DOM
                    backup_vnode = vnode.apply_to_dom(Some(backup_vnode), &mut entrypoints.item(0).unwrap(), s.clone(), &None);
                }
            });

}
        
    };
}