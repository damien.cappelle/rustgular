
mod component_traits;
mod types;
mod module_macro;
pub mod virtual_dom;
mod tools;
mod callback;
mod observable;
pub mod prelude;

pub mod request;

pub use component_traits::{Component, Render as ComponentRender, External, Callbackable};
pub use observable::Observable;
pub use callback::Callback;
pub use types::{ComponentID, CallbackPointer};

pub use rustgular_macros;
pub use js_sys;
pub use web_sys::{self, Element};
// pub use uuid;
use wasm_bindgen::prelude::*;
pub use wasm_bindgen;
pub use wasm_bindgen_futures::{self, spawn_local};

pub use wasm_bindgen::closure::Closure;
pub use async_std::{sync::{Arc, Mutex}, self};

pub use async_channel;
pub use console_error_panic_hook;

pub use virtual_dom::velement::VElement;

pub use async_std::future::Future;
pub use std::rc::Rc;

pub use async_trait::async_trait;

#[wasm_bindgen]
extern "C" {
    fn alert(s: &str);

    #[wasm_bindgen(js_namespace = console)]
    fn log(s: &str);

    #[wasm_bindgen(js_namespace = console)]
    fn error(s: &str);
}

pub fn console_log(stuff: &str) {
    log(stuff);
}

#[macro_export]
macro_rules! log {
    ($($arg:tt)*) => {{
        $crate::console_log(&format!($($arg)*));
    }};
}


pub fn console_error(stuff: &str) {
    error(stuff);
}

pub fn greet(name: &str) {
    alert(&format!("Super Hello, {}!", name));
}
