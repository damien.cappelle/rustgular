
use std::rc::Rc;
use std::fmt::Debug;
use crate::Callbackable;
use async_std::sync::{Arc, Mutex};
pub type ComponentID = u64;

#[derive(Clone)]
pub struct CallbackPointer<T> (pub Arc<Mutex<dyn Callbackable>>, pub Rc<dyn Fn(Arc<Mutex<dyn Callbackable>>, T) -> ()>);

impl<T: Debug> Debug for CallbackPointer<T> {

    fn fmt(&self, _f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        Ok(())
    }
}