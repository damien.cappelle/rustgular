use crate::VElement;
use crate::tools::TurnToAny;
use async_std::sync::{Arc, Mutex};
use async_trait::async_trait;
use crate::ComponentRender;

#[async_trait(?Send)]
pub trait External<S> 
    where Self: Sized 
{

    fn new(services: Arc<Mutex<S>>, trigger: async_channel::Sender<()>) -> Self;

    fn request_rendering(&mut self);

    /// Check wether component should be re-rendered
    /// When called, clear render flag
    fn should_be_rerendered(&mut self) -> bool;

    fn install_window_callbacks(&self, this: Arc<Mutex<Self>>, root: &mut VElement);

    async fn render(&self, this: Arc<Mutex<Self>>, services: Arc<Mutex<S>>, element: &mut VElement, children: &mut Vec<Box<dyn ComponentRender<VElem = VElement, S=S>>>);

    fn get_impersonation() -> Option<&'static str> {
        None
    }

    fn get_tag() -> &'static str;

    fn on_init(this: Arc<Mutex<Self>>);
}


pub trait Callbackable: TurnToAny
{
}

impl<T: TurnToAny + 'static> Callbackable for T {
}

pub trait FromDynCallbackable {
    fn from_dyn_callbackable(source: &mut dyn Callbackable) -> &mut Self;
}

impl<C: Callbackable + 'static> FromDynCallbackable for C {
    fn from_dyn_callbackable(source: &mut dyn Callbackable) -> &mut Self {
        source.as_mut_any().downcast_mut::<Self>().unwrap()
    }
}


#[macro_export]
macro_rules! try_with_services {
    ( $self:ident, $a:pat, $x:expr ) => {
        {
            let mut services_guard = $self.__rustgular_services.try_lock().expect("Couldn't lock services");
            let $a = &mut services_guard.deref_mut();
            $x
        }
    }
}

#[macro_export]
macro_rules! with_services {
    ( $self:ident, $a:pat, $x:expr ) => {
        {
            let mut services_guard = $self.__rustgular_services.lock().await;
            let $a = &mut services_guard.deref_mut();
            $x
        }
    }
}

#[macro_export]
macro_rules! trigger_rendering {
    ($self:ident) => {
        self.__rustgular_render_trigger.send(())
    }
}