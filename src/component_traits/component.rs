
use std::ops::DerefMut;

use crate::{component_traits::*, VElement};

use async_std::sync::{Arc, Mutex};
use async_trait::async_trait;


pub struct Component<A: External<S>, S> {
    pub app: Arc<Mutex<A>>,
    pub children: Vec<Box<dyn Render<VElem = VElement, S=S>>>,
    pub services: Arc<Mutex<S>>
}

impl<A: External<S> + 'static, S> Component<A, S> {

    pub fn new(services: Arc<Mutex<S>>, trigger: async_channel::Sender<()>) -> Self {
        let this = Self {
            app: Arc::from(Mutex::new(A::new(services.clone(), trigger))),
            children: vec![],
            services
        };

        A::on_init(this.app.clone());

        this
    }

    // For now, this function is useless
    pub fn  execute_with_app<F: Fn(&mut A)>(&self, func: F) {
        func(self.app.try_lock().unwrap().deref_mut());
    }


}


#[async_trait(?Send)]
pub trait Render: crate::tools::TurnToAny {
    type VElem;
    type S;
    async fn render(&mut self, element: &mut Self::VElem);

    fn get_tag(&self) -> &'static str;

    fn get_name(&self) -> &'static str {
        return std::any::type_name::<Self>();
    }
}

#[async_trait(?Send)]
impl<A: External<S> + 'static, S: 'static> Render for Component<A, S> {

    type VElem = VElement;
    type S = S;
    async fn render(&mut self, element: &mut VElement) {
        let this = self.app.clone();
        //let element = self.elem.clone();
        //let element = VElement::create()

        // If this component impersonates a specific tag, update it in virtual DOM
        if let Some(new_tag) = A::get_impersonation() {
            element.impersonate(new_tag);
        }

        // let this = this.try_lock().unwrap().deref_mut();
        
        let mut app_mutex_guard = this.lock().await;

        let this = app_mutex_guard.deref_mut();

        this.render(self.app.clone(), self.services.clone(), element, &mut self.children).await;
        
    }

    fn get_tag(&self) -> &'static str {
        A::get_tag()
    }
}