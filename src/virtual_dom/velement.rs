use wasm_bindgen::JsCast;
/** 
 * Implement virtual DOM
 * Based on React diff algorithm described here: https://legacy.reactjs.org/docs/reconciliation.html
 */

use web_sys::{Element, window, Document, Text};
use std::{collections::HashMap, hash::{Hasher, Hash}};
use crate::{Callback, CallbackPointer};
use async_channel;
use similar;

use crate::virtual_dom::vnode::{VNode, VNodeType};

#[derive(Debug, Clone)]
pub struct VElement {
    pub tag: String,
    pub children: Vec<VNode>,
    pub attributes: Vec<(String, String)>,
    pub classes: Vec<String>,
    _self_closed: bool,
    pub callbacks: Vec<(String, Callback)>,
    pub window_callbacks: HashMap<String, Callback>,
    pub namespace: Option<String>,
    pub viewchild_callback: Option<CallbackPointer<Element>>,
    pub forced_inner_html: Option<String>,
    /// Only for components: indicates whether a component is reused or is new
    /// This field must be checked before re-using callbacks
    pub component_is_reused: bool,
    pub style: Option<String>
}

impl Hash for VElement {
    fn hash<H: Hasher>(&self, state: &mut H) {
        // TODO: for now, callbacks are not hashed !
        self.tag.hash(state);
        self.children.hash(state);
        self.attributes.hash(state);
        self.classes.hash(state);
        self.namespace.hash(state);
        self.forced_inner_html.hash(state);
        self.style.hash(state);
    }
}


impl VElement {

    pub fn create(tag: &str, self_closed: bool, namespace: Option<&str>, viewchild_callback: Option<CallbackPointer<Element>>) -> Self {
        Self {
            tag: tag.to_string(),
            children: vec![],
            attributes: vec![],
            classes: vec![],
            _self_closed: self_closed,
            callbacks: vec![],
            window_callbacks: HashMap::new(),
            namespace: namespace.map(|ns| ns.to_string()),
            viewchild_callback,
            forced_inner_html: None,
            component_is_reused: false,
            style: None
        }
    }

    pub fn append_with_str(&mut self, content: &str) {
        self.children.push(VNode{
            node: VNodeType::Text(content.to_string()),
            hash_result: None
        });
    }

    pub fn add_element(&mut self, elem: VElement) {
        self.children.push(VNode{
            node: VNodeType::Element(elem),
            hash_result: None
        });
    }

    pub fn set_attribute(&mut self, attr: &str, value: &str) {
        self.attributes.push((attr.to_string(), value.to_string()));
    }
    
    pub fn record_callback(&mut self, event: &str, callback: CallbackPointer<web_sys::Event>) {
        self.callbacks.push((event.to_string(), Callback{
            event_type: event.to_string(),
            element: None,
            callback,
            handler: None
        }));
    }

    pub fn record_window_callback(&mut self, event: &str, callback: CallbackPointer<web_sys::Event>) {
        self.window_callbacks.insert(event.to_string(), Callback{
            callback,
            element: None,
            event_type: event.to_string(),
            handler: None
        });
    }

    pub fn clear(&mut self) {
        self.children.clear();
        self.callbacks.clear();
        self.window_callbacks.clear();
    }

    pub fn impersonate(&mut self, new_tag: &str) {
        self.tag = new_tag.to_string();
    }

    pub fn force_inner_html(&mut self, value: String) {
        self.forced_inner_html = Some(value);
    }

}



impl VElement {

    pub fn apply_to_dom(
        &mut self, 
        previous: Option<&mut Self>, 
        root: &mut Element, 
        render_trigger: async_channel::Sender::<()>,
        namespace: &Option<String>
    ) {
        let namespace = self.namespace.clone().or(namespace.clone());

        // If no previous version is provided, create an empty one
        let mut inexistant_previous = VElement::create(
                "inexistant", 
                false, 
                None, 
                None
        );
        let previous = if let Some(prev) = previous {
             prev 
        } else {
            &mut inexistant_previous
        };

        let document = window().unwrap().document().unwrap();

        match &self.forced_inner_html {
            None => {
                // Check what changed
                let old = previous.children.iter().map(|c| c.to_tag()).collect::<Vec<Option<String>>>();
                let new = self.children.iter().map(|c| c.to_tag()).collect::<Vec<Option<String>>>();
                let differences = similar::capture_diff_slices(similar::Algorithm::Myers, &old, &new);

                let mut dom_update_count: isize = 0;
                let mut previous_update_count: usize = 0;
                for diff in differences {
                    
                    match diff {

                        similar::DiffOp::Equal{old_index, new_index, len} => {
                            let children = root.child_nodes();
                            for i in 0..len {
                                let node = children.item(((old_index + i) as isize + dom_update_count) as u32)
                                    .expect(&format!(
                                        "Should be able to find html node while rendering\n Diff: {:?}\n prev: {:?}\n children: {:?}\n Parent Tag: {:?}", 
                                        (old_index + i) as isize + dom_update_count,
                                        previous.children[old_index + i - previous_update_count],
                                        children.length(),
                                        root.tag_name()
                                    ));
                                let child = self.children.remove(new_index + i).apply_to_dom(
                                    Some(previous.children.remove(old_index + i - previous_update_count)), 
                                    &mut node.unchecked_into(), 
                                    render_trigger.clone(), &namespace
                                );
                                previous_update_count += 1;
                                self.children.insert(new_index + i, child);
                            }
                        },
                        similar::DiffOp::Delete { old_index, old_len, new_index: _} => {
                            for i in old_index..(old_index + old_len) {
                                root.remove_child(&root.child_nodes().item((i as isize + dom_update_count) as u32).expect(&format!("Deleted item: should find item in DOM list: {}", i)))
                                    .expect("Can't remove deleted child");
                                dom_update_count -= 1;
                            }
                        },
                        similar::DiffOp::Replace { old_index, old_len, new_index, new_len } => {
                            // Remove all olds
                            for i in old_index..(old_index + old_len) {
                                root.remove_child(&root.child_nodes().item((i as isize + dom_update_count) as u32).expect("Should find item in DOM list"))
                                    .expect("Can't remove replaced child");
                                dom_update_count -= 1;
                            }

                            for i in new_index..(new_index + new_len) {
                                let child = VElement::create_dom_child(root, i as u32,self.children.remove(i), &namespace, &document, render_trigger.clone());
                                self.children.insert(i, child);
                                dom_update_count += 1;
                            }
                        },
                        similar::DiffOp::Insert { old_index: _, new_index, new_len } => {
                            for i in new_index..(new_index + new_len) {
                                let child = VElement::create_dom_child(root, i as u32, self.children.remove(i), &namespace, &document, render_trigger.clone());
                                self.children.insert(i, child);
                                dom_update_count += 1;
                            }
                        }
                    }
                }
            },
            Some(inner_html) => {
                if previous.forced_inner_html != self.forced_inner_html {
                    root.set_inner_html(&inner_html);
                }
            }
        }

        let window = web_sys::window().expect("Can't get Window element from web_sys");
        for (event_type, callback) in self.window_callbacks.iter_mut() {

            callback.install(&window, event_type);

        }

        // Only install new callbacks if dom element is new or component is new
        if !self.component_is_reused || previous.callbacks.is_empty() {

            for (event_type, callback) in self.callbacks.iter_mut() {
                callback.install(root, event_type);
            }
        } else {
            // Otherwise, take back previous callbacks (to save them from de-allocation)
            for _ in 0..previous.callbacks.len() {
                self.callbacks.clear();
                self.callbacks.push(previous.callbacks.remove(0));
            }
        }

        previous.callbacks.clear();

        // Apply attributes
        self.apply_attributes(&previous.attributes,root);

        // Apply classes
        root.set_attribute("class", self.classes.join(" ").as_str()).unwrap();

        // Apply styles
        if let Some(style) = &self.style {
            let elem = document.create_element("style").unwrap();
            elem.set_inner_html(style);
            root.prepend_with_node_1(&elem).unwrap();
        }

        // Only apply viewchild callbacks on new elements
        if previous.viewchild_callback.is_none() {
            self.play_viewchild_callback(root.clone());
        }
    }

    fn apply_attributes(&self, previous_attr: &[(String, String)], root: &mut Element) {

        for diff in similar::capture_diff_slices(similar::Algorithm::Myers, previous_attr, &self.attributes) {
            match diff {
                similar::DiffOp::Equal { old_index: _, new_index: _, len: _ } => {},
                similar::DiffOp::Insert { old_index: _, new_index, new_len } => {
                    for i in new_index..(new_index + new_len) {
                        root.set_attribute(&self.attributes[i].0, &self.attributes[i].1).expect("Should be able to apply attribute");
                    }
                },
                similar::DiffOp::Delete { old_index, old_len, new_index: _ } => {
                    for i in old_index..(old_index + old_len) {
                        root.remove_attribute(&previous_attr[i].0).expect("Can't remove old attribute");
                    }
                },
                similar::DiffOp::Replace { old_index, old_len, new_index, new_len } => {
                    for i in old_index..(old_index + old_len) {
                        root.remove_attribute(&previous_attr[i].0).expect("Can't remove old attribute");
                    }

                    for i in new_index..(new_index + new_len) {
                        root.set_attribute(&self.attributes[i].0, &self.attributes[i].1).expect("Can't add new attribute");
                    }
                }
                
            }
        }
    }

    /// Play recorded viewchild callbacks
    fn play_viewchild_callback(&self, element: Element) {
        if let Some(cb) = &self.viewchild_callback {
            let this = cb.0.clone();
            let cb_ptr = cb.1.clone();
            // let services = services.clone();
            // let mut app_mutex_guard = this.try_lock().unwrap();
            // let mut services_mutex_guard = services.try_lock().unwrap();
            // let this = app_mutex_guard.deref_mut();
            (cb_ptr)(this, element);

        }
    }

    fn create_dom_child(root: &mut Element, index: u32, mut child: VNode, namespace: &Option<String>, document: &Document, render_trigger: async_channel::Sender::<()>) -> VNode {

        let child_tag = child.to_tag();

        match child_tag {
            Some(tag) => {
                let namespace = child.unchecked_into_element().namespace.clone().or(namespace.clone());
                let mut element = {
                    match &namespace {
                        Some(ns) => document.create_element_ns(Some(&ns), &tag),
                        None => document.create_element(&tag)
                    }.expect(&format!("Can't create element {}", &tag))
                };
                child = child.apply_to_dom(None, &mut element, render_trigger.clone(), &namespace);
                root.insert_before(&element, root.child_nodes().item(index).as_ref()).expect("Can't insert new HTMLElement node");
            },
            None => {
                root.insert_before(&Text::new_with_data(child.unchecked_into_text()).unwrap().into(), root.child_nodes().item(index).as_ref())
                        .expect("Can't insert new text node");
            }            
        }

        child

    }
}
