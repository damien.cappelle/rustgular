
/** 
 * Implement virtual DOM
 * Based on React diff algorithm described here: https://legacy.reactjs.org/docs/reconciliation.html
 */

use web_sys::Element;
use std::{collections::hash_map::DefaultHasher, hash::{Hasher, Hash}};
use async_channel;

use crate::virtual_dom::velement::VElement;

#[derive(Debug, Clone)]
pub struct VNode {
    pub node: VNodeType,
    pub hash_result: Option<u64>
}

#[derive(Debug, Clone, Hash)]
pub enum VNodeType {
    Text(String),
    Element(VElement)
}

impl VNode {
    pub fn to_tag(&self) -> Option<String> {
        match &self.node {
            VNodeType::Element(c) => Some(c.tag.clone()),
            VNodeType::Text(_) => None
        }
    }

    pub fn unchecked_into_element(&mut self) -> &mut VElement {
        match &mut self.node {
            VNodeType::Element(c) => c,
            _ => panic!("Should be an element")
        }
    }

    pub fn unchecked_into_text(&mut self) -> &mut String {
        match &mut self.node {
            VNodeType::Text(c) => c,
            _ => panic!("Should be an element")
        }
    }

    pub fn hash_and_store(&mut self) -> u64 {
        if let Some(hash) = self.hash_result {
            hash
        } else {
            let mut s = DefaultHasher::new();
            
            match &mut self.node {
                VNodeType::Text(text) => text.hash(&mut s),
                VNodeType::Element(velem) => {
                    velem.tag.hash(&mut s);
                    for child in &mut velem.children {
                        child.hash_and_store().hash(&mut s);
                    }
                    velem.attributes.hash(&mut s);
                    velem.classes.hash(&mut s);
                    velem.namespace.hash(&mut s);
                    velem.forced_inner_html.hash(&mut s);
                    velem.style.hash(&mut s);
                }
            }
            let result = s.finish();
            self.hash_result = Some(result);
            result
        }
    }
}

/// Apply a virtual element to real DOM
/// # Parameters:
/// * `namespace`: Whether a namespace from parent should be applied
impl VNode {
    pub fn apply_to_dom(
        mut self, 
        mut previous: Option<Self>, 
        root: &mut Element, 
        render_trigger: async_channel::Sender::<()>,
        namespace: &Option<String>
    ) -> Self {
    

        let previous_hash = previous.as_mut().map_or(0, |pr| pr.hash_and_store());
        if previous_hash != self.hash_and_store() {
            match &mut self.node {
                VNodeType::Element(velem) => {
                    // Delete all existing children
                    let style_tags = root.get_elements_by_tag_name("style");
                    for i in 0..style_tags.length() {
                        root.remove_child(style_tags.item(i).unwrap().as_ref()).unwrap();
                    }

                    let previous = previous.as_mut().map(|pr| pr.unchecked_into_element());
                    velem.apply_to_dom(previous, root, render_trigger, namespace);
                },
                VNodeType::Text(text) => {root.set_text_content(Some(text.as_str()))}
            }
            self
        } else {
            // If dom is not modified, previous version of virtual dom is returned (to avoid copying all callback pointers)
            previous.expect("Previous should always exists if hash is the same as before")
        }
    }
}

impl Hash for VNode {
    fn hash<H: Hasher>(&self, state: &mut H) {

        if let Some(hash) = self.hash_result {
            hash.hash(state);
        } else {
            self.node.hash(state);
        }
    }
}