
use rustgular::prelude::*;
 
use crate::app_module;

#[rustgular::rustgular_macros::component(
    selector: "app-AppComponent",
    template: r#"<app-RecursiveComponent [children]="vec![6,5,4,3,2,1,0]"></app-RecursiveComponent>"#
    // template: r#"<app-RecursiveComponent [children]="vec![2000,1,0]"></app-RecursiveComponent>"#
)]
pub struct AppComponent {
} 


