
// mod app_module;
// mod app_component;
// mod recursive_component;
// use wasm_bindgen::prelude::*;


// #[wasm_bindgen]
// pub fn run() -> Result<(), JsValue> {
    
//     use std::panic;
//     panic::set_hook(Box::new(rustgular::console_error_panic_hook::hook));

//     app_module::initialize_entrypoint();

//     Ok(())
// }

// mod app_module {
//         use rustgular::{self, prelude::*};


//     pub use crate::app_component::AppComponent;
//     pub use crate::recursive_component::RecursiveComponent;

//     pub struct Services {        
//     }        

//     impl Services {
//         pub fn new() -> Self {
//             Self {
//             }
//         }
//     }

//     pub fn initialize_entrypoint() {}
// }

// #[cfg(test)]
// mod tests {

//     use rustgular::{self, prelude::*};

//     pub use crate::app_component::AppComponent;
//     pub use crate::recursive_component::RecursiveComponent;
//     use crate::app_module::Services;
//     use instant::Instant;
//     use rustgular::virtual_dom::{vnode::VNode, vnode::VNodeType};

//     use std::{collections::{HashMap, hash_map::DefaultHasher}, hash::{Hasher, Hash}};

//     use wasm_bindgen_test::*;
//     wasm_bindgen_test::wasm_bindgen_test_configure!(run_in_browser);
    
//     // #[derive(Debug, Default)]

//     async fn prepare() -> (Node, rustgular::Component::<AppComponent, Services>, rustgular::web_sys::Element, rustgular::async_channel::Sender::<()>) {
//         use std::panic;
//         panic::set_hook(Box::new(rustgular::console_error_panic_hook::hook));

//         let document = rustgular::web_sys::window().unwrap().document().unwrap();

//         // Install Entrypoint to body
//         document.get_elements_by_tag_name("body").item(0).unwrap().append_child(document.create_element("app-component").unwrap().as_ref()).unwrap();


//         // Look for entrypoint
//         let entrypoints = rustgular::web_sys::window().unwrap().document().unwrap().get_elements_by_tag_name("app-component");

//         if entrypoints.length() == 0 {
//             panic!("There should be only 1 entrypoint element");
//         }

//         let (s, _): (rustgular::async_channel::Sender::<()>, rustgular::async_channel::Receiver::<()>) = rustgular::async_channel::unbounded();
//         let services = rustgular::Arc::from(rustgular::Mutex::new(Services::new()));

//         let mut root_component = rustgular::Component::<AppComponent, Services>::new(services, s.clone());
        
//         // Empty virtual DOM root
//         let mut vnode = VNode{
//             node: NodeType::Element(rustgular::VElement::create("root", false, None, None)),
//             hash_result: None
//         };
        
//         // let services = services.clone();
//         root_component.render(&mut vnode.unchecked_into_element()).await;    

//         vnode.apply_to_dom(None, &mut entrypoints.item(0).unwrap(), s.clone(), &None);
    
//         (vnode, root_component, entrypoints.item(0).unwrap(), s)
//     }
    
//     #[wasm_bindgen_test]
//     async fn rerender_time() {

//         rustgular::log!("\nRerender Benchmark");
//         rustgular::log!("==================");

//         let (vnode, mut root_component, mut dom_entrypoint, s) = prepare().await;

//         let mut backup_vnode = vnode;

//         let start_time = Instant::now();
                
//         let mut vnode = Node{
//             node: NodeType::Element(rustgular::VElement::create("root", false, None, None)),
//             hash_result: None
//         };
//         root_component.render(&mut vnode.unchecked_into_element()).await;

//         // rustgular::log!("Hash: {:?} {:?}", backup_velem.hash_and_store(), velem.hash_and_store());

//         rustgular::log!("Rendering time: {:?}", start_time.elapsed());

//         // Update real DOM
//         vnode.apply_to_dom(Some(&mut backup_vnode), &mut dom_entrypoint, s.clone(), &None);
//         // velem.apply_to_dom(None, &mut dom_entrypoint, s.clone(), &None);


//         rustgular::log!("Total elapsed time: {:?}", start_time.elapsed());
//     }


//     // #[wasm_bindgen_test]
//     // async fn benchmark_diff() {

//     //     use similar;

//     //     let (velem, _, _, _) = prepare().await;

//     //     let velem = match &velem.children[0].node {
//     //         NodeType::Element(velem) => {
//     //             match &velem.children[0].node {
//     //                 NodeType::Element(velem) => {
//     //                     velem
//     //                 },
//     //                 _ => panic!("Won't happend")
//     //             }
//     //         },
//     //         _ => panic!("won't happend")
//     //     };

//     //     let start_time = Instant::now();
//     //     let old = velem.children.iter().map(|c| c.to_tag()).collect::<Vec<String>>();
//     //     let new = velem.children.iter().map(|c| c.to_tag()).collect::<Vec<String>>();
//     //     let differences = similar::capture_diff_slices(similar::Algorithm::Myers, &old, &new);
        
//     //     rustgular::log!("\nDIFF Benchmark");
//     //     rustgular::log!("==============");
//     //     rustgular::log!("Total diff time: {:?}", start_time.elapsed());
//     //     rustgular::log!("Amount of components to diff: {:?}", velem.children.len());
//     // } 

//     // #[wasm_bindgen_test]
//     // async fn benchmark_diff_single_element() {

//     //     use similar;

//     //     let (velem, _, _, _) = prepare().await;

//     //     let velem = match &velem.children[0].node {
//     //         NodeType::Element(velem) => {
//     //             match &velem.children[0].node {
//     //                 NodeType::Element(velem) => {
//     //                     match &velem.children[0].node {
//     //                         NodeType::Element(velem) => velem.clone(),
//     //                         _ => panic!("")
//     //                     }
//     //                 },
//     //                 _ => panic!("Won't happend")
//     //             }
//     //         },
//     //         _ => panic!("won't happend")
//     //     };

//     //     let start_time = Instant::now();
//     //     let old = velem.children.iter().map(|c| c.to_tag()).collect::<Vec<String>>();
//     //     let new = velem.children.iter().map(|c| c.to_tag()).collect::<Vec<String>>();
//     //     let alloc_time = start_time.elapsed();
//     //     let differences = similar::capture_diff_slices(similar::Algorithm::Lcs, &old, &new);
        
//     //     rustgular::log!("\nDIFF Benchmark");
//     //     rustgular::log!("==============");
//     //     rustgular::log!("Total diff time: {:?}", start_time.elapsed());
//     //     rustgular::log!("Alloc time: {:?}", alloc_time);
//     //     rustgular::log!("Amount of components to diff: {:?}", velem.children.len());
//     // } 
//     // #[wasm_bindgen_test]
//     // async fn benchmark_equal() {

//     //     let (mut velem, _, dom_entrypoint, s) = prepare().await;

//     //     let root = dom_entrypoint.children().item(0).unwrap();

//     //     let mut velem = match &mut velem.children[0].node {
//     //         NodeType::Element(velem) => {
//     //             match velem.children.remove(0).node {
//     //                 NodeType::Element(velem) => {
//     //                     velem
//     //                 },
//     //                 _ => panic!("Won't happend")
//     //             }
//     //         },
//     //         _ => panic!("won't happend")
//     //     };

//     //     let mut previous = velem.clone();

//     //     let start_time = Instant::now();
//     //     let children = root.child_nodes().item(0).unwrap().child_nodes();
//     //     rustgular::log!("Amount of nodes: {:?}", children.length());

//     //     let mut total_single_apply_to_node_time = 0.0; 
//     //     let mut amount_of_apply_to_node_calls = 0;
//     //     for i in 0..velem.children.len() {
//     //         let node = children.item(i as u32).unwrap();
//     //         match &mut velem.children[i].node {
            
//     //             NodeType::Element(elem) => {
//     //                 let start_time = Instant::now();
//     //                 // elem.apply_to_dom(Some(previous.children[i].unchecked_into_element()), &mut node.unchecked_into(), s.clone(), &None);
//     //                 elem.apply_to_dom(Some(previous.children[i].unchecked_into_element()), &mut node.unchecked_into(), s.clone(), &None);

//     //                 total_single_apply_to_node_time += start_time.elapsed().as_secs_f64();
//     //                 amount_of_apply_to_node_calls += 1;
//     //             },
//     //             NodeType::Text(text) => {
//     //                 if node.text_content().as_mut() != Some(text) {
//     //                     node.set_text_content(Some(text));
//     //                 }
//     //             }
//     //         }
//     //     }

//     //     rustgular::log!("\nEQUAL Benchmark");
//     //     rustgular::log!("==============");
//     //     rustgular::log!("Total diff time: {:?}", start_time.elapsed());
//     //     rustgular::log!("Mean apply to node time: {:?} us", 1e6 * total_single_apply_to_node_time / (amount_of_apply_to_node_calls as f64));

//     // }

//     // #[wasm_bindgen_test]
//     // async fn benchmark_hash() {

//     //     use similar;

//     //     let (mut velem, _, _, _) = prepare().await;

//     //     let mut velem = match &mut velem.children[0].node {
//     //         NodeType::Element(velem) => {
//     //             velem.children[0].clone()
//     //         },
//     //         _ => panic!("won't happend")
//     //     };

//     //     let start_time = Instant::now();
//     //     let mut s = DefaultHasher::new();
//     //     velem.hash(&mut s);
//     //     s.finish();
//     //     let hash_time = start_time.elapsed();

//     //     let start_time = Instant::now();
//     //     velem.hash_and_store();
//     //     let hash_and_store_time = start_time.elapsed();

//     //     rustgular::log!("\nHASH Benchmark");
//     //     rustgular::log!("==============");
//     //     rustgular::log!("Total hash time: {:?}", start_time.elapsed());
//     //     rustgular::log!("Total hash-and-store time:  {:?}", start_time.elapsed());
//     // } 

// }
