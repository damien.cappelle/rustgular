
use rustgular::prelude::*;
 
use crate::app_module;

#[rustgular::rustgular_macros::component(
    selector: "app-RecursiveComponent",
    template: r#"
    <div *if-pattern="!self.children.is_empty()">
    <div *for="let i in 0..self.children[0]">
    {{ i }}
    <app-RecursiveComponent [children]="self.get_children()"></app-RecursiveComponent>
    </div>
    </div>
"#
)]
pub struct RecursiveComponent {
    #[input]
    children: Vec<usize>
}

impl RecursiveComponent {
    fn get_children(&self) -> Vec<usize> {
        self.children[1..].iter().cloned().collect()       
    }
}
