
use std::ops::DerefMut;

use crate::app_module;
use rustgular::prelude::*;


#[rustgular::rustgular_macros::component(
    selector: "first-test", 
    template: r#"<button (click)="this.add_component()">{{ self.message.clone() }}</button><div *for="let a in self.table.iter()">{{a}}<app-SecondTestComponent></app-SecondTestComponent></div>"#)]
pub struct FirstTestComponent {
    table: Vec<String>,
    counter: usize,
    #[input]
    message: String,
}

// #[rustgular::rustgular_macros::component_impl]
impl FirstTestComponent {


    // #[callback]
    fn add_component<'a>(&'a mut self) {

        let counter = self.counter;
        self.table.push(counter.to_string());
        self.counter += 1;
    }
}