
use crate::app_module;
use rustgular::with_services;
use std::ops::DerefMut;

#[rustgular::rustgular_macros::component(
    selector: "second-test",
    template: "<h2>Second test</h2><button (click)=\"async{this.increment_counter().await}\">Click me {{ self.counter }} </button>")]
pub struct SecondTestComponent {
    counter: usize
}

// #[rustgular::rustgular_macros::component_impl]
impl SecondTestComponent {

    async fn increment_counter(&mut self) {
        with_services!(self, s, {
            self.counter = s.service_example.access();
        });
    }
}




