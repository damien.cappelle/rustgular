
use rustgular::{self, prelude::*};

use crate::service_example::ServiceExample;
pub use crate::{first_test_component::FirstTestComponent, second_test_component::SecondTestComponent, app_component::AppComponent};

rustgular::module!{service_example => ServiceExample ; AppComponent}
