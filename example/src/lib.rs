
mod app_module;
mod app_component;
mod first_test_component;
mod second_test_component;
mod service_example;
pub use wasm_bindgen::prelude::*;

#[wasm_bindgen]
pub fn initialize() {
    app_module::initialize_entrypoint();
}