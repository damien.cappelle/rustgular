

#[derive(Debug, Default)]
pub struct ServiceExample {
    access_counter: usize
}

impl ServiceExample {

    pub fn access(&mut self) -> usize {
        self.access_counter += 1;
        self.access_counter
    }
}

