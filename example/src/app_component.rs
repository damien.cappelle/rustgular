
use crate::app_module;
use rustgular::prelude::*;

#[rustgular::rustgular_macros::component(
    selector: "app-component", 
    template: r#"Let's test this !<app-FirstTestComponent [message]="self.name.clone()"></app-FirstTestComponent><app-FirstTestComponent ></app-FirstTestComponent>"#
)]
// #[rustgular::rustgular_macros::component("app-component", r#"<a></a>"#)]
pub struct AppComponent {
    name: String
}

